"use client";

// import ReactDOM from 'react-dom';
import {QRCodeSVG} from 'qrcode.react';

  export default async function Page() {
    
    
    
    return (
      <div className="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
    QR
        <QRCodeSVG value="https://reactjs.org/" />,
      </div>
  );
}




// // With promises
// QRCode.toDataURL('I am a pony!')
//   .then(url => {
//     console.log(url)
//   })
//   .catch(err => {
//     console.error(err)
//   })

// // With async/await
// const generateQR = async text => {
//   try {
//     console.log(await QRCode.toDataURL(text))
//   } catch (err) {
//     console.error(err)
//   }
// }