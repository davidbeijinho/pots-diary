import "server-only";
import { Inter } from "next/font/google";

const inter = Inter({ subsets: ["latin"] });

import "./globals.css";

export const revalidate = 0;
import PocketBaseProvider from "@/components/provider/pocketbase-provider";

export default async function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {

  return (
    <html lang="en">
      {/*
        <head /> will contain the components returned by the nearest parent
        head.tsx. Find out more at https://beta.nextjs.org/docs/api-reference/file-conventions/head
      */}
      <head />
      <body className={inter.className}>
        <PocketBaseProvider >
          {children}
        </PocketBaseProvider>
      </body>
    </html>
  );
}
