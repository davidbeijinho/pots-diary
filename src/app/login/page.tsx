import TitleLayout from "@/components/TitleLayout";
import LoginForm from "@/components/loginForm";

export default async function Page() {
  return (
    <TitleLayout title="Login">
      <div className="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
        <LoginForm />
      </div>
    </TitleLayout>
  );
}
