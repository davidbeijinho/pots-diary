import TitleLayout from "@/components/TitleLayout";
import LogoutForm from "@/components/logoutForm";

export default async function Page() {
  return (
    <TitleLayout title="Logout">
      <div className="mx-auto max-w-screen-xl px-4 py-16 sm:px-6 lg:px-8">
        <LogoutForm />
      </div>
    </TitleLayout>
  );
}
