import { notFound } from "next/navigation";
import PostImage from "@/components/postImage";
import TitleLayout from "@/components/TitleLayout";
import UpdateForm from "@/components/updateForm";
import UpdatesTimeline from "@/components/updatesTimeline";
import { pocketBase } from "@/lib/pocketbase";

async function getData(params: {id: string}) {
  const pot = await pocketBase.collection("pots").getOne(params.id);
  if (!pot) {
    return { pot: null, updates: null };
  }

  const images = pot.images.map((name:string) => {
    return pocketBase.files.getUrl(pot, name);
  });

  const updates = await pocketBase.collection("updates").getFullList({
    sort: "-created",
    queryParams: `pot = ${pot.id}`,
  });

  return {
    images,
    pot,
    updates,
  };
}

export default async function Page({ params }: { params: { id: string } }) {
  const { pot, updates, images } = await getData(params);

  if (!pot) {
    notFound();
  }

  return (
    <TitleLayout title={`Pot: ${pot.name}`}>
      <PostImage path={images?.[0]}/>
      <div className="container max-w-6xl mx-auto -mt-32">
        <div className="mx-0 sm:mx-6">
          <div
            className="bg-white w-full p-8 md:p-24 text-xl md:text-2xl text-gray-800 leading-normal"
            style={{ fontFamily: "Georgia,serif" }}
          >
            <p
              className="text-2xl md:text-3xl mb-5"
              dangerouslySetInnerHTML={{ __html: pot.description }}
            ></p>
            {/* <p className="py-6">
              The basic blog page layout is available and all using the default
              Tailwind CSS classes (although there are a few hardcoded style
              tags). If you are going to use this in your project, you will want
              to convert the classes into components.
            </p> */}
          </div>
        </div>
      </div>
      {/* 
        <pre>{JSON.stringify({ params }, null, 2)}</pre>
        <pre>{JSON.stringify({ pot }, null, 2)}</pre>
        <pre>{JSON.stringify({ updates }, null, 2)}</pre> 
      */}
      <div className="container  max-w-6xl mx-auto ">
        <hr />
        <p className="text-2xl md:text-3xl m-5">👋 Updates</p>
        {updates && <UpdatesTimeline updates={updates} />}
      </div>
      <UpdateForm pot={params.id} />
    </TitleLayout>
  );
}
