import TitleLayout from "@/components/TitleLayout";
import PotsGrid from "@/components/potsGrid";
import { pocketBase } from "@/lib/pocketbase";

export default async function Pots() {
  const pots = await pocketBase.collection("pots").getFullList({
    sort: "-created",
  });
  return (
    <TitleLayout title="Pots">
      {/* <pre>{JSON.stringify(pots, null, 2)}</pre>; */}
      <PotsGrid pots={pots} />
    </TitleLayout>
  );
}
