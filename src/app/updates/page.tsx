import PostImage from "@/components/postImage";
import TitleLayout from "@/components/TitleLayout";
import UpdatesTimeline from "@/components/updatesTimeline";
import { pocketBase } from "@/lib/pocketbase";

export default async function Updates() {
  const updates = await pocketBase.collection("updates").getFullList({
    sort: "-created",
  });
  const first = updates?.at(0)
  const firstImage = first?.images?.[0]
  const image = firstImage ? pocketBase.files.getUrl(first, firstImage) : undefined;

  return (
    <TitleLayout title="Updates">
      <PostImage path={image} />
      <div className="container max-w-6xl mx-auto -mt-32">
        <div className="mx-0 sm:mx-6">
          <div
            className="bg-white w-full p-8 md:p-24 text-xl md:text-2xl text-gray-800 leading-normal"
            style={{ fontFamily: "Georgia,serif" }}
          >
            <p className="text-2xl md:text-3xl mb-5">👋 Some updates here</p>
          </div>
        </div>
        {/* <pre>{JSON.stringify({ updates }, null, 2)}</pre> */}
        {/* {updates?.map((update) => (
          <Update update={update} />
        ))} */}
      </div>
      <div className="container  max-w-6xl mx-auto ">
        {updates && <UpdatesTimeline updates={updates} />}
      </div>
    </TitleLayout>
  );
}
