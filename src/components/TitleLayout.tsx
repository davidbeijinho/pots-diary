import BaseLayout from "@/components/baseLayout";
import Title from "@/components/title";

export default function TitleLayout({
  children,
  title,
}: {
  children: React.ReactNode;
  title: string;
}) {
  return (
    <BaseLayout>
      <Title title={title} />
      {children}
      {/* <Footer /> */}
    </BaseLayout>
  );
}
