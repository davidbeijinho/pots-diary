import Nav from "@/components/nav";

export default function BaseLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <>
      <Nav />
      {children}
      {/* <Footer /> */}
    </>
  );
}
