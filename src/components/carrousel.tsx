"use client";

import { Carousel } from "flowbite-react";
import { useEffect, useState } from "react";
import CarrouselImage from "./carrouselImage";

import { usePocketBase } from "@/components/provider/pocketbase-provider";

export default function Carrousel({ update }: { update: any }) {
  const { pocketBase } = usePocketBase();

  const [files, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const data = update.images.map((name:string) => {
        return pocketBase.files.getUrl(update, name);
      });
      setData(data);
    };

    fetchData().catch(console.error);
  }, [update]);
  if (files?.length !== 0) {
    return (
      <div className="relative h-64 overflow-hidden rounded-lg sm:h-80 lg:order-last lg:h-full">
        <div className="h-56 sm:h-64 xl:h-80 2xl:h-96">
          <Carousel>
            {files.map((file) => (
              <CarrouselImage
                path={file}
                key={file}
              />
            ))}
          </Carousel>
        </div>
      </div>
    );
  }
  return <></>;
}
