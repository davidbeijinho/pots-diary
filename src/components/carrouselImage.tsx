import Image from 'next/image'

export default function CarrouselImage({ path }: { path: string }) {
  return <img src={path} alt="..." className="h-56 sm:h-64 xl:h-80 2xl:h-96 mx-auto" /> ;
}
