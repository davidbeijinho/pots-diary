"use client";

import { usePocketBase } from "@/components/provider/pocketbase-provider";

const LoginNavButton = () => {
  const { login } = usePocketBase();
  return login ? (
    <a
      href="/logout"
      className="inline-block text-gray-600 no-underline hover:text-gray-200 hover:text-underline py-2 px-2"
    >
      Logout
    </a>
  ) : (
    <>
      <a
        href="/login"
        className="inline-block text-gray-600 no-underline hover:text-gray-200 hover:text-underline py-2 px-2"
      >
        Login
      </a>
    </>
  );
};

export default LoginNavButton;
