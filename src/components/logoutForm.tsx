"use client";

import { usePocketBase } from "@/components/provider/pocketbase-provider";

const LogoutForm = () => {
  const { pocketBase, login } = usePocketBase();

  const handleLogout = async () => {
    console.log("LOGOUT", login, pocketBase);
    const error = await pocketBase.authStore.clear();

    if (error) {
      console.log({ error });
    }
  };

  if (login) {
    return (
      <button
        className="inline-block rounded-lg bg-blue-500 px-5 py-3 text-sm font-medium text-white"
        onClick={handleLogout}
      >
        Logout
      </button>
    );
  }
  return (
    <>
      You are logout | do you want to <a href="/login">login</a>
    </>
  );
};

export default LogoutForm;
