export default function PostImage({ path = 'https://source.unsplash.com/collection/1118905/' }: {  path?: string  }) {
  return (
    <div
      className="container w-full max-w-6xl mx-auto bg-white bg-cover mt-8 rounded"
      style={{
        backgroundImage:
          `url(${path})`,
        height: "75vh",
      }}
    ></div>
  );
}
