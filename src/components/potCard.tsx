import { Record } from "pocketbase";
import Image from 'next/image'

export default function PotCard({ pot }: { pot: Record }) {
  return (
    <div className="w-full md:w-1/3 px-2 pb-4">
      <div className="h-full bg-white rounded overflow-hidden shadow-md hover:shadow-lg relative smooth">
        <a href={`/pots/${pot.id}`} className="no-underline hover:no-underline">
          <img
            alt="pot image"
            src="https://source.unsplash.com/_AjqGGafofE/400x200"
            className="h-48 w-full rounded-t shadow-lg"
          />
          <div className="p-6 h-auto">
            {/* <p className="text-gray-600 text-xs md:text-sm">GETTING STARTED</p> */}
            <div className="font-bold text-xl text-gray-900">{pot.name}</div>
            <p className="text-gray-800 font-serif text-base mb-5" dangerouslySetInnerHTML={{__html: pot.description}}>
            </p>
          </div>
          {/* <div className="flex items-center justify-between inset-x-0 bottom-0 p-6">
            <img
              className="w-8 h-8 rounded-full mr-4"
              src="http://i.pravatar.cc/300"
              alt="Avatar of Author"
            />
            <p className="text-gray-600 text-xs md:text-sm">2 MIN READ</p>
          </div> */}
        </a>
      </div>
    </div>
  );
}
