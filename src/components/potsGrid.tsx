import { Record } from "pocketbase";
import PotCard from "./potCard";

export default function PotsGrid({pots}:{pots: Record[]}) {
  return (
    <div className="">
      <div className="container w-full max-w-6xl mx-auto px-2 py-8">
        <div className="flex flex-wrap -mx-2">
          {pots.map((pot)=> <PotCard pot={pot} key={pot.id} />)}
        </div>
      </div>
    </div>
  );
}
