// // import type { Session } from '@supabase/auth-helpers-nextjs';
// import { createContext, useContext } from 'react';
// // import type { TypedSupabaseClient } from '@/app/layout';
// // import { createBrowserClient } from '@/lib/supabase-browser';

// // type MaybeSession = Session | null;

// type PocketBaseContext = {
//   pocketBase: any;
//   session: any;
// };

// // @ts-ignore
// const Context = createContext<PocketBaseContext>();

// import PocketBase from 'pocketbase';


// export default function PocketBaseProvider({
//   children,
//   session
// }: {
//   children: React.ReactNode;
//   session: any;
// }) {
//   const pocketBase = new PocketBase('https://pocketbase.hive.thebeijinho.com')

//   return (
//     <Context.Provider value={{ pocketBase, session }}>
//       <>{children}</>
//     </Context.Provider>
//   );
// }

// export const usePocketBase = () => useContext(Context);
