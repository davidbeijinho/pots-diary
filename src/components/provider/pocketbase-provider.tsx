"use client";

import { createContext, useContext, useState } from "react";
import { pocketBase as pb } from '@/lib/pocketbase';

type PocketBaseContext = {
  pocketBase: any;
  login: Boolean;
};

const Context = createContext<PocketBaseContext>({
  pocketBase: {},
  login: false,
});

export default function PocketBaseProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  const [pocketBase] = useState(
    () => pb
  );
  const [login, setLogin] = useState(() => pocketBase.authStore.isValid);

  pocketBase.authStore.onChange((token, model, ...rest) => {
    console.log("New store data:", token, model, rest);
    if (token) {
      setLogin(pocketBase.authStore.isValid)
    } else {
      setLogin(false)

    }
  }, false);

  return (
    <Context.Provider value={{ pocketBase, login }}>
      <>{children}</>
    </Context.Provider>
  );
}

export const usePocketBase = () => useContext(Context);
