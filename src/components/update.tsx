import Carrousel from "@/components/carrousel";
import { Record } from "pocketbase";

export default function Update({ update }: { update: Record }) {
  const time = new Date(update.created).toLocaleString("en-GB", {
    timeZone: "UTC",
  });

  return (
    <section>
      <div className="mx-auto max-w-screen-xl px-4 py-8 sm:py-12 sm:px-6 lg:py-16 lg:px-8">
        <div className="grid grid-cols-1 gap-8 lg:grid-cols-2 lg:gap-16">
          <Carrousel update={update} />
          <div className="lg:py-24">
            <h2 className="text-3xl font-bold sm:text-4xl">{update?.title}</h2>
            <p>{time}</p>
            <p className="mt-4 text-gray-600" dangerouslySetInnerHTML={{__html: update.content}}></p>
          </div>
        </div>
      </div>
    </section>
  );
}
