"use client";

import { usePocketBase } from "@/components/provider/pocketbase-provider";
import { useRouter } from "next/navigation";
import Title from "@/components/title";

export default function UpdateForm({ pot }: { pot: string }) {
  const { pocketBase, login } = usePocketBase();
  const router = useRouter();

  if (!login) {
    return <p>Login to add update</p>;
  }

  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    const form = event.target;
    // @ts-ignore
    const formData = new FormData(form);
    console.log({ event, target: event.target, formData });
    const title = formData.get("title");
    const content = formData.get("content");
    const images = formData.get("images");

    if (title && content && images) {
      const response = await pocketBase.collection('updates').create(formData);

      if (response.id) {
        router.refresh();
      } else {
        console.log({ response });
      }
    }
  };

  return (
    <form
      onSubmit={handleSubmit}
      className="mx-auto mb-0 mt-8 max-w-md space-y-4"
    >
      <hr />
      <Title title="add update" />
      <div>
        <label htmlFor="title" className="sr-only">
          Title
        </label>

        <div className="relative">
          <input
            type="title"
            name="title"
            id="title"
            className="w-full rounded-lg border-gray-200 p-4 pe-12 text-sm shadow-sm"
            placeholder="Enter title"
          />
        </div>
      </div>
      <div>
        <label htmlFor="content" className="sr-only">
          Content
        </label>

        <div className="relative">
          <textarea
            name="content"
            id="content"
            className="w-full rounded-lg border-gray-200 p-4 pe-12 text-sm shadow-sm"
            placeholder="Enter content"
          />
        </div>
      </div>
      <div>
        <label htmlFor="password" className="sr-only">
          File
        </label>

        <div className="relative">
          <input
            type="file"
            name="images"
            accept="image/*;capture=camera"
            id="images"
            multiple
            className="w-full rounded-lg border-gray-200 p-4 pe-12 text-sm shadow-sm"
          />
        </div>
      </div>
      <div className="flex items-center justify-between">
        <button
          type="submit"
          className="inline-block rounded-lg bg-blue-500 px-5 py-3 text-sm font-medium text-white"
        >
          Create
        </button>
      </div>
    </form>
  );
}
