// "use client";

// import { useSupabase } from "@/components/provider/supabase-provider";
// import { useRouter } from "next/navigation";

// export default function UpdateImageForm({ updateId }: { updateId: string }) {
//   const { supabase, session } = useSupabase();
//   const router = useRouter();

//   if (!session) {
//     return <p>Login to add image</p>;
//   }

//   const handleSubmit = async (event: React.FormEvent) => {
//     event.preventDefault();
//     const form = event.target;
//     // @ts-ignore
//     const formData = new FormData(form);
//     console.log({ event, target: event.target, formData });
//     const file = formData.get("file");
//     if (file) {
//       const { data, error } = await supabase.storage
//         .from("updates-images")
//         // @ts-ignore
//         .upload(`updates/${updateId}/image-${file.name}`, file, {
//           cacheControl: "3600",
//           upsert: false,
//         });

//       if (error) {
//         console.log({ error });
//       } else {
//         router.refresh();
//       }
//     }
//   };

//   return (
//     <form
//       onSubmit={handleSubmit}
//       className="mx-auto mb-0 mt-8 max-w-md space-y-4"
//     >
//       <div>
//         <label htmlFor="password" className="sr-only">
//           File
//         </label>

//         <div className="relative">
//           <input
//             type="file"
//             name="file"
//             accept="image/*;capture=camera"
//             id="file"
//             className="w-full rounded-lg border-gray-200 p-4 pe-12 text-sm shadow-sm"
//             placeholder="Enter password"
//           />
//         </div>
//       </div>
//       <div className="flex items-center justify-between">
//         <button
//           type="submit"
//           className="inline-block rounded-lg bg-blue-500 px-5 py-3 text-sm font-medium text-white"
//         >
//           Upload
//         </button>
//       </div>
//     </form>
//   );
// }
