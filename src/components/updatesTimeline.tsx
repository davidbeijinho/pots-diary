"use client";

import { Timeline } from "flowbite-react";
import UpdatesTimelineItem from "@/components/updatesTimelineItem";
import { Record } from "pocketbase";
export default function UpdatesTimeline({updates}:{updates: Record[]}) {
  return (
    <Timeline>
       {updates.map((update)=> <UpdatesTimelineItem update={update} key={update.id} />)}
    </Timeline>
  );
}
