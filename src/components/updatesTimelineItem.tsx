import { Timeline } from "flowbite-react";
import Carrousel from "@/components/carrousel";
import { Record } from "pocketbase";
export default function updatesTimelineItem({
  update,
}: {
  update: Record;
}) {
  const time = new Date(update.created).toLocaleString("en-GB", {
    timeZone: "UTC",
  });

  return (
    <Timeline.Item>
      <Timeline.Point />
      <Timeline.Content>
        <Timeline.Time>{time}</Timeline.Time>
        <Timeline.Title>{update?.title}</Timeline.Title>
        <Timeline.Body dangerouslySetInnerHTML={{__html: update.content}}></Timeline.Body>
        <Carrousel update={update} />
      </Timeline.Content>
    </Timeline.Item>
  );
}
