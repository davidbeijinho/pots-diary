export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json }
  | Json[]

export interface Database {
  public: {
    Tables: {
      updates: {
        Row: {
          content: string | null
          created_at: string | null
          id: number
          images: Json | null
          pot: number
          title: string
        }
        Insert: {
          content?: string | null
          created_at?: string | null
          id?: number
          images?: Json | null
          pot: number
          title: string
        }
        Update: {
          content?: string | null
          created_at?: string | null
          id?: number
          images?: Json | null
          pot?: number
          title?: string
        }
      }
      vasos: {
        Row: {
          created_at: string | null
          description: string | null
          id: number
          images: Json | null
          name: string | null
        }
        Insert: {
          created_at?: string | null
          description?: string | null
          id?: number
          images?: Json | null
          name?: string | null
        }
        Update: {
          created_at?: string | null
          description?: string | null
          id?: number
          images?: Json | null
          name?: string | null
        }
      }
    }
    Views: {
      [_ in never]: never
    }
    Functions: {
      [_ in never]: never
    }
    Enums: {
      [_ in never]: never
    }
    CompositeTypes: {
      [_ in never]: never
    }
  }
}
